// Fonction jQuery : exécute le jeu une fois que le DOM est chargé

$(function initialize() {



	/* ---------- ---------- */
	/* ----- SETTINGS ------ */
	/* ---------- ---------- */

	// Objet littéral qui stocke l'argent, les vies et la vitesse du jeu


	var	Player = {
			money: 60,
			life : 10,
			speed: 0, // 10 = fast; 50 = normal mode
			time : 10, // time (in sec) before monsters move
			level: 1,
		}; 
	

	/* ---------- ---------- */
	/* ------ PARCOURS ----- */
	/* ---------- ---------- */

	// Objet littéral qui stocke le parcours des monstres


	var	Parcours = {
			start: 1500, 
			sizeCourse: 150,
			course: [
				['down' ,600],
				['left' ,1030],
				['down' ,0],
			]
		};

	// On appelle la fonction qui crée le parcours (visuel)
	makeCourse(Parcours);

	/* ---------- ---------- */
	/* ------ TOWERS ------- */
	/* ---------- ---------- */

	var towers = [];  // Tableau qui stocke toutes les tours du jeu

	// On affiche les tours que l'on peut créer à l'écran
	displayTowers(Player, towers); 

	// On appelle la fonction qui permet de créer des tours
	makeTowers(towers, Player);

	/* ---------- ---------- */
	/* ----- MONSTERS ------ */
	/* ---------- ---------- */

	var	monsters = []; // Tableau qui stocke tous les monstres du jeu

	// On appelle la fonction qui permet de créer des monstres

	/* ---------- ---------- */
	/* ------- GAME -------- */
	/* ---------- ---------- */

	// On appelle la fonction qui lance le jeu
	startGame(Player, Parcours, monsters, towers);

})


// ------------------------------------------------------------------------- //
// ----------------------- ALL FUNCTIONS FOR THE GAME ---------------------- //
// ------------------------------------------------------------------------- //

// ----------------------
// --- FUNCTIONS GAME ---
// ----------------------

// Fonction qui déclare les monstres à créer et les stocke dans le tableau des monstres



function makeMonstersBoss(Player, monsters, Parcours) {
	var MonsterToCreateBoss;

	// On crée l'ensemble des monstres que l'on stocke dans un tableau
	for (var i = 0, max = 1; i < max; i++) {
		// On crée un monstre
		MonsterToCreateBoss = new Monster(-100*(i+1), Parcours.start, 300+(10*Player.level), 'Creeper', 30, 'resources/Creeper.png');
		monsters.push(MonsterToCreateBoss);
	}
}

function makeMonsters(Player, monsters, Parcours) {
	var MonsterToCreate;

	// On crée l'ensemble des monstres que l'on stocke dans un tableau
	for (var i = 0, max = 5; i < max; i++) {
		// On crée un monstre
		MonsterToCreate = new Monster(-100*(i+1), Parcours.start, 100+(10*Player.level), 'Pillager', 2+Player.level, 'resources/pillager.png');
		monsters.push(MonsterToCreate);
	}
}

// Fonction qui lance le jeu
function startGame(Player, Parcours, monsters, towers) {
	// On affiche les informations du joueur (html)
	$('.infos span.time').text(Player.time);
	$('.infos span.life').text(Player.life);
	$('.infos span.money').text(Player.money);
	$('.infos span.level').text(Player.level);
	var pseudo = document.getElementById("pseudo").value ; 
	$('.infos span.pseudo').text(pseudo);
	console.log(pseudo);

	if (document.getElementById('number_people_2').checked) {
	 Player.speed = 10;
	 	console.log(Player.speed)

	};
	if (document.getElementById('number_people_4').checked) {
	 Player.speed = 5;
	 	console.log(Player.speed)

	};
	if (document.getElementById('number_people_6').checked) {
	 Player.speed = 1;
	 	console.log(Player.speed)

	};
	// On lance le décompte
	var timer = setInterval(function() {
		$('.infos span.time').text(Player.time); // On change chaque seconde le temps restant
		if (Player.time <= 0) {

			// On arrête le décompte
			clearInterval(timer);


			// On lance le timer pour déplacer les monstres et attaquer
			monsterMove(Player, Parcours, monsters, towers, Player.speed);
			makeMonsters(Player, monsters, Parcours)

		}

		else {
			Player.time--;
		}


	}, 1000);

}


function startBoss(Player, Parcours, monsters, towers) {
	// On affiche les informations du joueur (html)
	$('.infos span.time').text(Player.time);
	$('.infos span.life').text(Player.life);
	$('.infos span.money').text(Player.money);
	$('.infos span.level').text(Player.level);



	// On lance le décompte
	var timer = setInterval(function() {
		$('.infos span.time').text(Player.time); // On change chaque seconde le temps restant
		if (Player.time <= 0) {

			// On arrête le décompte
			clearInterval(timer);

			// On lance le timer pour déplacer les monstres et attaquer
			monsterMove(Player, Parcours, monsters, towers, Player.speed);
			makeMonstersBoss(Player, monsters, Parcours)

		}

		else {
			Player.time--;
		}


	}, 1000);

}




// ----------------------
// -- FUNCTIONS OTHERS --
// ----------------------

// Fonction qui calcule l'hypotenuse
function calcHypotenuse(a, b) {
  return(Math.sqrt((a * a) + (b * b)));
}

// Fonction qui retourne une valeur comprise en % d'un chiffre
function hpPourcent (hp, hpMax) {
	return parseInt(hp * 100 / hpMax);
}



const name = document.getElementById('name');
const form = document.getElementById('booking-form');

var accueil = document.getElementById('accueil');
var jeu = document.getElementById('Jeu');

jeu.style.display = "none" ;

form.addEventListener('submit', (e) => {
	let messages = [];
	e.preventDefault();

	$("#Jeu").fadeIn(5000);
	$("#accueil").fadeOut(1000);
})

